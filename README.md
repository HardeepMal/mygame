### INTRODUCTION ###

Horror Story game, is a user interface or chatbot application where I have set some questions and answers based on those. So, when user start this interface, they have to answer some questions and based on their answers they will get further options. At the end, either they will be able to win or loss the game.

### TECHNOLOGY  ###

* Node JS
* JavaScript
* Text Editor


### REQUIRED SOFTWARE ###

* Visual Studio Code - https://code.visualstudio.com/download
* Node.js - https://nodejs.org/en/download/
* Other guidelines

### LICENCE ###
Copyright 2020 @HardeepKaur.This application is a open source project where ANYONE CAN USE, IMPROVE IT AND CAN HAVE FULL ACCESS TO THE CODE WITHOUT ANY CONCERN OR COPYRIGHT ISSUES.